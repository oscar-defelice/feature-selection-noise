# Feature selection by adding Noise

The aim of this repository is to collect an example of a feature selection technique: _i.e._ iteratively remove features that are less important than random noise.

Here we use Gaussian noise, but other kind of noise might be more suitable according to the application.

--- 

## Introduction

The purpose of this small library is to apply feature selection to your high-dimensional data. In order to do that, we apply the following steps:

1. Input features are automatically standardised
2. a column containing gaussian noise (average = 0, standard deviation = 1) is added
3. a model is trained
4. the feature importance is evaluated
5. all the features that are less important than the random one, are excluded. 

The previous steps are repeated iteratively, until the algorithm converges.

## Initialize the repository
Let us start by cloning the repository, by using the following command:

```bash
git clone git@gitlab.com:oscar-defelice/feature-selection-noise.git
```

Then you need to install the dependencies. I suggest to create a virtual environment, as follows:

```bash
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
```

To check if everything works, you can run the unit tests:

```bash
python -m pytest -v tests
```

You are now ready to use the repository!

## Getting started
The example you need is contained in [this notebook](notebooks/example.ipynb).
A toy dataset, to build a regression model, is imported. 
Then we import the function `get_relevant_features`
```
from src.ml import get_relevant_features
```
This function takes as arguments:
- `features`
- `labels`
- `model`, a scikit-learn model
- `epochs`, the number of epochs (i.e. for how many cycles you want to apply recursively the feature selection)
- `patience`, number of epochs without any improvement of the features selection, before stopping the process (the idea is similar to the early stopping of Tensorflow/Keras)
- `splitting_type`, it can be equal to `simple` (for simple train/test split) or `kfold` (for 5-fold splitting). If you choose `kfold`, the feature importance will be computed as the average feature importance for each train/test subset.
- `filename_output`, a string to indicate where to save the file. You can also choose `False` if you do not want to save it
- `random_state`, set the random seed that it is used by the k-fold splitting

The function `get_relevant_features` returns a DataFrame with a reduced dataset, i.e. a dataset that contains only the most important features.


## References

The most important paper for this repository is available [here](https://www.jmlr.org/papers/volume3/stoppiglia03a/stoppiglia03a.pdf).
There is a nice python implementation that follows the interface of scikit-learn, called [**Boruta**](https://github.com/scikit-learn-contrib/boruta_py) deriving from a paper available [here](https://www.jstatsoft.org/article/view/v036i11).